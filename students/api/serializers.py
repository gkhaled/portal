from __future__ import unicode_literals

import rest_framework.serializers

from .. import models


class MarkSerializer(rest_framework.serializers.ModelSerializer):
    class Meta:
        model = models.Mark
        fields = ('grade', 'course', 'user', )


class StudentSerializer(rest_framework.serializers.ModelSerializer):
    class Meta:
        model = models.User
        fields = ('email', 'first_name', 'last_name', )


class CourseSerializer(rest_framework.serializers.ModelSerializer):
    class Meta:
        model = models.Course
        fields = ('course_name', )