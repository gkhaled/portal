from __future__ import unicode_literals

from django.db.models import Avg, Max, Min

import rest_framework.generics
import rest_framework.permissions
import rest_framework.response

from .. import models
from . import serializers


class CreateMarkView(rest_framework.generics.CreateAPIView):
    serializer_class = serializers.MarkSerializer
    permission_classes = (rest_framework.permissions.IsAuthenticated, )


class RetrieveStudentAverageGrade(rest_framework.generics.RetrieveAPIView):
    serializer_class = serializers.StudentSerializer
    permission_classes = (rest_framework.permissions.IsAuthenticated, )
    queryset = models.User.objects.all()

    def retrieve(self, request, *args, **kwargs):
        # get_object() will validate object not found + permissions above, id will got from <pk> in the url
        student = self.get_object()
        average_grade = student.marks.aggregate(Avg('grade'))
        return rest_framework.response.Response(data={
            'average_grade': average_grade['grade__avg'],
        })


class RetrieveCourseAverageGrade(rest_framework.generics.RetrieveAPIView):
    serializer_class = serializers.CourseSerializer
    permission_classes = (rest_framework.permissions.IsAuthenticated, )
    queryset = models.Course.objects.all()

    def retrieve(self, request, *args, **kwargs):
        # get_object() will validate object not found + permissions above, id will got from <pk> in the url
        course = self.get_object()
        average_grade = course.all_course_marks.aggregate(Avg('grade'))
        return rest_framework.response.Response(data={
            'average_grade': average_grade['grade__avg'],
        })


class RetrieveStudentHighestGrade(rest_framework.generics.RetrieveAPIView):
    serializer_class = serializers.StudentSerializer
    permission_classes = (rest_framework.permissions.IsAuthenticated, )
    queryset = models.User.objects.all()

    def retrieve(self, request, *args, **kwargs):
        # get_object() will validate object not found + permissions above, id will got from <pk> in the url
        student = self.get_object()
        max_grade = student.marks.aggregate(Max('grade'))
        return rest_framework.response.Response(data={
            'highest_grade': max_grade['grade__max'],
        })


class RetrieveStudentLowestGrade(rest_framework.generics.RetrieveAPIView):
    serializer_class = serializers.StudentSerializer
    permission_classes = (rest_framework.permissions.IsAuthenticated, )
    queryset = models.User.objects.all()

    def retrieve(self, request, *args, **kwargs):
        # get_object() will validate object not found + permissions above, id will got from <pk> in the url
        student = self.get_object()
        min_grade = student.marks.aggregate(Min('grade'))
        return rest_framework.response.Response(data={
            'lowest_grade': min_grade['grade__min'],
        })