from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^v1/mark/create/', views.CreateMarkView.as_view(), name='create-mark'),

    url(r'^v1/student/grade/average/(?P<pk>\d+)/', views.RetrieveStudentAverageGrade.as_view(), name='student-avg-grade'),
    url(r'^v1/student/grade/highest/(?P<pk>\d+)/', views.RetrieveStudentHighestGrade.as_view(), name='course-max-grade'),
    url(r'^v1/student/grade/lowest/(?P<pk>\d+)/', views.RetrieveStudentLowestGrade.as_view(), name='course-min-grade'),

    url(r'^v1/course/grade/average/(?P<pk>\d+)/', views.RetrieveCourseAverageGrade.as_view(), name='course-avg-grade'),
]