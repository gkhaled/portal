from __future__ import unicode_literals

import django.db.models
import django.contrib.auth.models
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone


class UserManager(django.contrib.auth.models.BaseUserManager):
    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        """
        Creates and saves a User with the given username, email and password.
        """
        if not email:
            raise ValueError(_('The given email must be set'))
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email=None, password=None, **extra_fields):
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError(_('Superuser must have is_superuser=True.'))

        return self._create_user(email, password, **extra_fields)


class User(django.contrib.auth.models.AbstractBaseUser, django.contrib.auth.models.PermissionsMixin):
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ('first_name', 'last_name',)

    first_name = django.db.models.CharField(_('first name'), max_length=30, blank=True)
    last_name = django.db.models.CharField(_('last name'), max_length=30, blank=True)
    is_staff = django.db.models.BooleanField(
        _('staff status'),
        default=False,
        help_text=_('Designates whether the user can log into this admin site.'),
    )
    is_active = django.db.models.BooleanField(
        _('active'),
        default=True,
        help_text=_(
            'Designates whether this user should be treated as active. '
            'Unselect this instead of deleting accounts.'
        ),
    )
    date_joined = django.db.models.DateTimeField(_('date joined'), default=timezone.now)
    email = django.db.models.EmailField(_('Email'), unique=True)

    objects = UserManager()

    def get_short_name(self):
        return self.first_name


class Course(django.db.models.Model):
    created_on = django.db.models.DateTimeField(auto_now_add=True)
    updated_on = django.db.models.DateTimeField(auto_now=True)

    course_name = django.db.models.CharField(_('Course Name'), max_length=100)


class Mark(django.db.models.Model):
    created_on = django.db.models.DateTimeField(auto_now_add=True)
    updated_on = django.db.models.DateTimeField(auto_now=True)

    grade = django.db.models.IntegerField(_('Grade'))
    course = django.db.models.ForeignKey(Course, related_name='all_course_marks')
    user = django.db.models.ForeignKey(User, related_name='marks')

    class Meta:
        # user can't have multiple marks for same course
        unique_together = ('course', 'user', )