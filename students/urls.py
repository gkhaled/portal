from __future__ import unicode_literals

import django.conf.urls


urlpatterns = [
    django.conf.urls.url(r'^api/', django.conf.urls.include('students.api.urls')),
]