from __future__ import unicode_literals

import django.contrib.admin

from . import models


@django.contrib.admin.register(models.User)
class UserAdmin(django.contrib.admin.ModelAdmin):
    pass


@django.contrib.admin.register(models.Course)
class CourseAdmin(django.contrib.admin.ModelAdmin):
    pass


@django.contrib.admin.register(models.Mark)
class MarkAdmin(django.contrib.admin.ModelAdmin):
    pass