from __future__ import unicode_literals

import django.test

import rest_framework.test


class StudentTestCase(django.test.TestCase):
    fixtures = ('user_marks.json', )

    def setUp(self):
        self.unauthenticated_client = rest_framework.test.APIClient()
        self.client = rest_framework.test.APIClient()
        self.client.credentials(HTTP_AUTHORIZATION='Token bcf09b767c858467ed82ea30731c30bc239ebca0')

    def test_auth_create_mark(self):
        self.unauthenticated_client = rest_framework.test.APIClient()
        r = self.unauthenticated_client.post('/api/v1/mark/create/', data = {
            'grade': 90,
            'course': 3,
            'user': 2
        })
        self.assertEqual(r.status_code, 401)

    def test_auth_students_avg(self):
        r = self.unauthenticated_client.get('/api/v1/student/grade/average/1/')
        self.assertEqual(r.status_code, 401)

    def test_auth_students_min(self):
        r = self.unauthenticated_client.get('/api/v1/student/grade/lowest/1/')
        self.assertEqual(r.status_code, 401)

    def test_auth_students_max(self):
        r = self.unauthenticated_client.get('/api/v1/student/grade/highest/1/')
        self.assertEqual(r.status_code, 401)

    def test_create_mark(self):
        r = self.client.post('/api/v1/mark/create/', data={
            'grade': 90,
            'course': 3,
            'user': 2
        })
        self.assertEqual(r.status_code, 201)

    def test_create_duplicate_marks(self):
        r = self.client.post('/api/v1/mark/create/', data={
            'grade': 90,
            'course': 3,
            'user': 2
        })
        self.assertEqual(r.status_code, 201)
        r = self.client.post('/api/v1/mark/create/', data={
            'grade': 85,
            'course': 3,
            'user': 2
        })
        self.assertEqual(r.status_code, 400)

    def test_student_avg(self):
        r = self.client.get('/api/v1/student/grade/average/1/')
        self.assertEqual(r.status_code, 200)
        self.assertEqual(r.data['average_grade'], 85)

    def test_student_min(self):
        r = self.client.get('/api/v1/student/grade/lowest/1/')
        self.assertEqual(r.status_code, 200)
        self.assertEqual(r.data['lowest_grade'], 80)

    def test_student_max(self):
        r = self.client.get('/api/v1/student/grade/highest/1/')
        self.assertEqual(r.status_code, 200)
        self.assertEqual(r.data['highest_grade'], 90)


class CourseTestCase(django.test.TestCase):
    fixtures = ('user_marks.json', )

    def setUp(self):
        self.unauthenticated_client = rest_framework.test.APIClient()
        self.client = rest_framework.test.APIClient()
        self.client.credentials(HTTP_AUTHORIZATION='Token bcf09b767c858467ed82ea30731c30bc239ebca0')

    def test_auth_course_avg(self):
        r = self.unauthenticated_client.get('/api/v1/course/grade/average/1/')
        self.assertEqual(r.status_code, 401)

    def test_course_avg(self):
        r = self.client.get('/api/v1/course/grade/average/1/')
        self.assertEqual(r.status_code, 200)
        self.assertEqual(r.data['average_grade'], 80)